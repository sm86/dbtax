# README #
This repository is an attempt to reproduce the code and bring DBtax back into production. Hence, improving DBpedia Ontology.

[Unsupervised Learning of an Extensive and Usable Taxonomy for DBpedia by Marco Fossati, Dimitris Kontokostas & Jens Lehmann](http://jens-lehmann.org/files/2015/semantics_dbtax.pdf). 

### About DBtax ###

DBpedia tries to extract structured information from Wikipedia and make information available on the Web. In this way, the DBpedia project develops a gigantic source of knowledge. However, the current system for building DBpedia Ontology relies on Infobox extraction. Infoboxes, being human curated, limit the coverage of DBpedia. This occurs either due to lack of Infoboxes in some pages or over-specific or very general taxonomies. These factors have motivated the need for DBTax. 

DBTax follows an unsupervised approach to learning taxonomy from the Wikipedia category system. It applies several inter-disciplinary NLP techniques to assign types to DBpedia entities. The success of the DBTax project is that it has extended the coverage of DBpedia from 2.2 million to 4.2 million records (as per the DBpedia 3.9 release). 


Maven build.